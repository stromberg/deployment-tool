# deployment-tool
soo what is this for tool? well the aim is a deployment tool! 

initially it will be to serve my purposes to roll my own 'distro' without to much manual labor, 
so i can control the rate of change and contents..

## software, is useless until it's deployed!
no matter if deployment target is fedora-X, fedora-Y, freebsd-R, ubuntu-Z or Alpine Linux y.xz

## deployed, deploy what?
a deployment is still just packaged code in different forms ready to be used.

## deploy as files, or with repositories, packages, package managers etc?
its still just a bunch of modules packaged together, in different stages, in some cases there might be downstream patching etc.

## without a specified use case, it will be hard to deduce what goes where and prioritize..
so use cases are important for deployment, need a htpc, well there is dedicated distros for that too!
then there is yocto, now we're in a embedded usecase if you need software for your gadget..

## so why place it on invent.kde.org?
well one thing im tired of with Gitlab-CI is the fact that its a mess to test software against its dependencies,
dependencies change too! either you only support the latest of dep-x and dep-y or there will be bumps in the road.

everything change, and still everything is pretty much the same.. its a bumpy road!

## use case for KDE? (who knows maybe reality by 2028 or 2038..)
a CI-distro! no more works on ubuntu or freebsd etc. 
the interesting question is does it work with freebsd-libc, glibc, musl, llvm-libc, bionic (google android) etc.
next layer adds more dependencies on top.. 

eventually we might get of the distro version of "big ball of mud" pattern 
